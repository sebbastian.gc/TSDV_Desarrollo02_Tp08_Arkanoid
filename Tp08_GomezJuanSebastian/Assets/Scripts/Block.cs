﻿
using UnityEngine;

public class Block : MonoBehaviour
{
    private short _score = 10;
    public delegate void BlockDestroy();
    public static BlockDestroy OnBlockDestroy;
    
    private void OnCollisionEnter(Collision other)
    {
        if (!other.gameObject.CompareTag("Ball")) return;
        DestroyBlock();
               
    }

    private void DestroyBlock()
    {
        GameManager.Get().score += _score;
        GameManager.Get().blocks--;

        if (GameManager.Get().blocks <= 0)
            GameManager.Get().gameOver = true;
        OnBlockDestroy?.Invoke();
        Destroy(gameObject);   
    }

}
