﻿using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float speed;
    private Rigidbody _rb;
    private Vector3 movementInput;
    private Vector3 initialPosition;
    
    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        initialPosition = transform.position;
        Ball.OnBallDestroy += ResetPosition;
    }

    private void FixedUpdate()
    {
       Move();
    }

    private void Update()
    {  
       
        movementInput = Vector3.zero;
        if (Input.GetKey(KeyCode.A))
            movementInput.x = -1;
        
        else if (Input.GetKey(KeyCode.D))
            movementInput.x = 1;
    }

    private void Move()
    { 
        if (!GameManager.Get().isMoving) return;
        _rb.MovePosition(_rb.position+movementInput.normalized * (speed * Time.fixedDeltaTime));
    }

    private void ResetPosition()
    {
        transform.position = initialPosition;
    }

    private void OnDestroy()
    {
        Ball.OnBallDestroy -= ResetPosition;
    }
}
