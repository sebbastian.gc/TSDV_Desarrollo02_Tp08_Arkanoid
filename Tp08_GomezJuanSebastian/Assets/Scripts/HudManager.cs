﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class HudManager : MonoBehaviour
{
    public Text textScore;
    public Text textHighScore;
    public RawImage lifeOne;
    public RawImage lifeTwo;
    public RawImage lifeThree;   
    public GameObject screenGameOver;
    
    private void Start()
    {
        GameManager.Get().score = 0;
        textHighScore.text= "HighScore: "+ PlayerPrefs.GetInt("highScore", 0);
        textScore.text = "Score:" + GameManager.Get().score;
        Ball.OnBallDestroy += HudUpdate;
        Block.OnBlockDestroy += HudUpdate;
        Ball.OnBallDestroy += GameOver;
        Block.OnBlockDestroy += GameOver;
    }
   
    public void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
        GameManager.Get().ResetGame();
    }

    private void HudUpdate() 
    {      
        textScore.text = "Score:" + GameManager.Get().score;
        CurrentLife();
    }

    private void CurrentLife()
    {   
        switch (GameManager.Get().life)
        {
            case 0:
                lifeOne.enabled = false;
                lifeTwo.enabled = false;
                lifeThree.enabled = false;
                break;
            case 1:
                lifeOne.enabled = true;
                lifeTwo.enabled = false;
                lifeThree.enabled = false;
                break;
            case 2:
                lifeOne.enabled = true;
                lifeTwo.enabled = true;
                lifeThree.enabled = false;
                break;
            case 3:
                lifeOne.enabled = true;
                lifeTwo.enabled = true;
                lifeThree.enabled = true;
                break;
        }
    }

    private void GameOver()
    {
        if (!GameManager.Get().gameOver) return;
        HighScore(GameManager.Get().score);
        screenGameOver.SetActive(true);

    }
    
    private void HighScore(int score)
    { 
        int highScore = PlayerPrefs.GetInt("highScore", 0);
        if (score > highScore)
        {
            PlayerPrefs.SetInt("highScore", score);
        }
        textHighScore.text = "HighScore: " + PlayerPrefs.GetInt("highScore", 0);
    }

    private void OnDisable()
    {
        Ball.OnBallDestroy -= HudUpdate;
        Block.OnBlockDestroy -= HudUpdate;
        Ball.OnBallDestroy -= GameOver;
        Block.OnBlockDestroy -= GameOver;
    }
}
