﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject screenCredits;
 
    public void ChangeScene(string scene)
    {
        GameManager.Get().gameOver = false;       
        SceneManager.LoadScene(scene);
    }
    public void Exit()
    {
        Application.Quit();
    }
}