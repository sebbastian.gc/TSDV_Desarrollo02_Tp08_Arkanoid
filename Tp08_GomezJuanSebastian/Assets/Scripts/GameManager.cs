﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

public class GameManager : MonoBehaviour
{
    public bool gameOver;
    public short score;
    public short life;
    public int blocks;
    public bool isMoving;
    private static GameManager instance;
    
    public static GameManager Get()
    {
        return instance;
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void ResetGame()
    {
        life = 3;
        score = 0;
        gameOver = false;
        
    }
}
