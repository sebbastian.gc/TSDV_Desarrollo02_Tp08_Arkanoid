﻿using System;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private Vector3 InitialPosition;

    public delegate void BallDestroy();
    public static BallDestroy OnBallDestroy;
    private Rigidbody _rb;
    private Vector3 direction;
    [SerializeField] private float speed;
    private float speedForce;
    

    private void Awake()
    {
        InitialPosition = transform.position;
        _rb = GetComponent<Rigidbody>();
        direction.x = 1;
        direction.y = 1;
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Update()
    {
        if (!Input.GetKeyDown(KeyCode.Space)) return;
        GameManager.Get().isMoving = true;

    }

    private void Move()
    {
        if (!GameManager.Get().isMoving) return;
        _rb.MovePosition(_rb.position+direction.normalized* ((speed+ speedForce) * Time.fixedDeltaTime));
    }

    private void OnCollisionEnter(Collision other)
    {
      
        string tag = other.gameObject.tag;
        switch (tag)
        { 
            case "Player":
                speedForce++;
                Collision(other);
                break;
            case "Limits":
            case "Block":
                Collision(other);
                break;
        }
    }

    private void Collision(Collision other)
    {
        ContactPoint contac =  other.contacts[0];
        if (contac.normal.x != 0)
            direction.x = contac.normal.x;
        if (contac.normal.y != 0)
            direction.y = contac.normal.y;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Back")) return;
        GameManager.Get().isMoving = false;
        GetComponent<MeshRenderer>().enabled = false;
        transform.position = InitialPosition;
        GameManager.Get().life--;
        speedForce = 0;
        
        if (GameManager.Get().life <= 0)
           GameManager.Get().gameOver = true;
        OnBallDestroy?.Invoke();
        GetComponent<MeshRenderer>().enabled = true;
        
    }
}
