﻿using System;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject blockPrefab;
    public Transform blockContainer;
    [SerializeField] private short rows;
    [SerializeField] private short columns;
    [SerializeField] private float offsetRows;
    [SerializeField] private float offsetColumns;
    private Vector3 position;

    private void Awake()
    {
        position = blockPrefab.transform.position;
        GameManager.Get().blocks = rows * columns;
        SpawnBlocks();
    }
    private void SpawnBlocks()
    {
        for (short i = 0; i < rows; i++)
        {
            for (short j = 0; j < columns; j++)
            {
                if(j!=0)
                    position.x += blockPrefab.transform.localScale.x + offsetColumns;
                if(i!=0)
                  position.y = (blockPrefab.transform.position.y - (offsetRows*i)) - (blockPrefab.transform.localScale.y *i );
                if (i != 0 && j == 0)
                    position.x = blockPrefab.transform.position.x;
               
                Instantiate(blockPrefab, position, transform.rotation,blockContainer);
            }
        }
    
    
    }
}
